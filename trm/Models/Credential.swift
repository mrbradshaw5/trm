//
//  Credential.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import Foundation
import UIKit

protocol CredentialDelegate: class {
    func saveComplete(success: Bool)
    func imageRetrieved(success: Bool)
    func imageReplaced(success: Bool)
}

struct Credential {
    weak var delegate: CredentialDelegate?
    
    var id: String?
    var isOrg: Bool?
    var title: String!
    var idNumber: String?
    var expiration: Date?
    var frontImageURL: String?
    var frontImageData: Data?
    var backImageURL: String?
    var backImageData: Data?
    var hasNotifications: Bool?
    var notifyDaysInAdvance: String?
}

extension Credential {
    func save() {
        
    }
    
    func retrieveImage(placement: ImagePlacement.Type, completion: @escaping (_ image: UIImage?) -> Void) {
    }
    
    func replaceImage(placement: ImagePlacement.Type) {
        
    }
    
    func delete() {
        
    }
}
