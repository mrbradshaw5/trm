//
//  CredentialManager.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import Foundation


protocol CredentialsDelegate: class {
    func credentialsDownloadedFromServer()
    func credentialsLoaded(credentials: Dictionary<String,[Credential]>?)
}

class CredentialManager {
    static let shared = CredentialManager()
    weak var delegate: CredentialsDelegate?
    
    private var credentials: [Credential] = []
    
    func getCredentials() {
        #warning("Remove Test Data")
        let creds: [Credential] = TestData.shared.returnTestData()
//        var creds = [Credential]()
//        credentials = []
        
        
        credentials = creds
        self.delegate?.credentialsDownloadedFromServer()
    }
    
    func returnCredentials(ofType: CredentialType, sortedBy: CredentialSort, filtered: Dictionary<String,[Credential]>? = nil) {
        var sortedCreds: [Credential] = []
        if let filteredCreds = filtered {
            let filtOrgArr = filteredCreds[CredTypeKey.org] ?? []
            let filtPerArr = filteredCreds[CredTypeKey.personal] ?? []
            sortedCreds = self.sortCredentials(credentials: filtOrgArr + filtPerArr, sortedBy: sortedBy)
        } else {
            sortedCreds = self.sortCredentials(credentials: credentials, sortedBy: sortedBy)
        }
        
        let organizedCreds = self.organizeCredentials(credentials: sortedCreds, ofType: ofType)
        
        self.delegate?.credentialsLoaded(credentials: organizedCreds)
    }
    
    private func sortCredentials(credentials: [Credential], sortedBy: CredentialSort) -> [Credential] {
        var creds = credentials
        switch sortedBy {
        case .titleAscending:
            creds.sort { (credOne, credTwo) -> Bool in
                let cred1 = credOne.title ?? ""
                let cred2 = credTwo.title ?? ""
                return cred1 < cred2
            }
        case .titleDescending:
            creds.sort { (credOne, credTwo) -> Bool in
                let cred1 = credOne.title ?? ""
                let cred2 = credTwo.title ?? ""
                return cred1 > cred2
            }
        case .expirationDescending:
            creds.sort { (credOne, credTwo) -> Bool in
                let cred1 = credOne.expiration ?? Date.distantPast
                let cred2 = credTwo.expiration ?? Date.distantPast
                return cred1 > cred2
            }
        case .expirationAscending:
            creds.sort { (credOne, credTwo) -> Bool in
                let cred1 = credOne.expiration ?? Date.distantPast
                let cred2 = credTwo.expiration ?? Date.distantPast
                return cred1 < cred2
            }
        }
        return creds
    }
    
    private func organizeCredentials(credentials: [Credential], ofType: CredentialType) -> Dictionary<String,[Credential]>? {
        var credDict: Dictionary<String,[Credential]> = [:]
        let orgArray = credentials.filter({(credential: Credential) -> Bool in
            return credential.isOrg == true
        })
        let perArray = credentials.filter({(credential: Credential) -> Bool in
            return credential.isOrg == false
        })
        
        switch ofType {
        case .all:
            if !orgArray.isEmpty {
                credDict[CredTypeKey.org] = orgArray
            }
            if !perArray.isEmpty {
                credDict[CredTypeKey.personal] = perArray
            }
        case .organization:
            if !orgArray.isEmpty {
                credDict[CredTypeKey.org] = orgArray
            }
        case .personal:
            if !perArray.isEmpty {
                credDict[CredTypeKey.personal] = perArray
            }
        }
        return credDict
    }
}
