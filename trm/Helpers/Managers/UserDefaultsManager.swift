//
//  UserDefaultsManager.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import Foundation

class UserDefaultsManager {
    static let shared = UserDefaultsManager()
    
    var defaultCredentialType: CredentialType? {
        get {
            guard let defaultCredentialType = UserDefaults.standard.value(forKey: UserDefaultsKeys.type) as? String else {
                return nil
            }
            return CredentialType(rawValue: defaultCredentialType)
        }
        set(defaultCredentialType) {
            UserDefaults.standard.set(defaultCredentialType?.rawValue, forKey: UserDefaultsKeys.type)
        }
    }
    
    var defaultCredentialSort: CredentialSort? {
        get {
            guard let defaultCredentialSort = UserDefaults.standard.value(forKey: UserDefaultsKeys.sort) as? String else {
                return nil
            }
            return CredentialSort(rawValue: defaultCredentialSort)
        }
        set(defaultCredentialSort) {
            UserDefaults.standard.set(defaultCredentialSort?.rawValue, forKey: UserDefaultsKeys.sort)
        }
    }
    
}
