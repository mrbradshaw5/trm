//
//  Date.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import Foundation

extension Date {
    var asString: String {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("MMM-dd-yyyy")
        return formatter.string(from: self)
    }
}
