//
//  CredentialKeys.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/24/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import Foundation

enum CredTypeKey {
    static let org = "Organization"
    static let personal = "Personal"
}
