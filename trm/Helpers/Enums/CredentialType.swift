//
//  CredentialType.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import Foundation

enum CredentialType: String, CaseIterable {
    case all
    case organization
    case personal
}
