//
//  UserDefaultsKeys.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import Foundation

enum UserDefaultsKeys {
    static let type = "type"
    static let sort = "sort"
}
