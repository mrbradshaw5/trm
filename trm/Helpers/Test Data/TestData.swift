//
//  TestData.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/24/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import Foundation


class TestData {
    static let shared = TestData()
    let today = Date()
    
    func returnTestData() -> [Credential] {
        let cred1 = Credential(id: "asdfhjkashdf",
                                     isOrg: true,
                                     title: "Test 1 ORG",
                                     idNumber: "1234",
                                     expiration: today.addingTimeInterval(86400 * 2),
                                     hasNotifications: false)
        
        let cred2 = Credential(id: "123",
                                     isOrg: true,
                                     title: "Test 2 ORG",
                                     idNumber: "4321",
                                     expiration: today.addingTimeInterval(86400 * 3),
                                     hasNotifications: false)
        
        
        let cred3 = Credential(id: "1234",
                                     isOrg: false,
                                     title: "Test 3 PER",
                                     idNumber: "12345",
                                     expiration: today.addingTimeInterval(86400 * 4),
                                     hasNotifications: false)
        
        let cred4 = Credential(id: "12345",
                                     isOrg: false,
                                     title: "Test 4 PER",
                                     idNumber: "123456",
                                     expiration: today.addingTimeInterval(86400 * 5),
                                     hasNotifications: false)
        
        let cred5 = Credential(id: "321",
                                     isOrg: true,
                                     title: "Test 5 ORG",
                                     idNumber: "1234567",
                                     expiration: today.addingTimeInterval(86400 * 6),
                                     hasNotifications: false)
        
        
        return [cred1, cred2, cred3, cred4, cred5]
    }
}
