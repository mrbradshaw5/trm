//
//  HomeVC.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    // MARK: - Properties
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    private var allCredentials: Dictionary<String, [Credential]>?
    private var filteredCredentials: Dictionary<String, [Credential]>?
    private var credential: Credential?
    private let credentialManager = CredentialManager.shared
    private var type: CredentialType = .all
    private var sortBy: CredentialSort = .expirationDescending
    
    // MARK: - Outlets
    @IBOutlet weak var info: UIButton!
    @IBOutlet weak var credentialTypeControl: UISegmentedControl!
    @IBOutlet weak var search: UITextField!
    @IBOutlet weak var add: UIButton!
    @IBOutlet weak var sortCredentialControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Actions
    @IBAction func infoButton(_ sender: Any) {
//        self.present(UIStoryboard.infoVC, animated: true)
    }
    @IBAction func typeSelected(_ sender: Any) {
        typeSelected()
    }
    @IBAction func seachField(_ sender: Any) {
        searchCredentials()
    }
    @IBAction func addButton(_ sender: Any) {
//        self.present(UIStoryboard.addVC, animated: true)
    }
    @IBAction func sortSelected(_ sender: Any) {
        sortSelected()
    }
    
    
    
    // MARK: - View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        credentialManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        credentialManager.getCredentials()
    }
    
    
    // MARK: - Methods
    private func typeSelected() {
        switch credentialTypeControl.selectedSegmentIndex {
        case 0:
            type = .all
        case 1:
            type = .organization
        case 2:
            type = .personal
        default:
            type = .all
        }
        UserDefaultsManager.shared.defaultCredentialType = type
        credentialManager.returnCredentials(ofType: type, sortedBy: sortBy)
    }
    
    private func searchCredentials() {
        // Do search things
    }
    
    private func isFiltering() -> Bool {
        // If not filtering
        filteredCredentials = nil
        
        return false
    }
    
    private func sortSelected() {
        switch sortCredentialControl.selectedSegmentIndex {
        case 0:
            sortBy = .titleAscending
        case 1:
            sortBy = .titleDescending
        case 2:
            sortBy = .expirationDescending
        case 3:
            sortBy = .expirationAscending
        default:
            sortBy = .expirationDescending
        }
        UserDefaultsManager.shared.defaultCredentialSort = sortBy
        credentialManager.returnCredentials(ofType: type, sortedBy: sortBy)
    }
}


extension HomeVC: CredentialsDelegate {
    func credentialsDownloadedFromServer() {
        credentialManager.returnCredentials(ofType: type, sortedBy: sortBy)
    }
    
    
    func credentialsLoaded(credentials: Dictionary<String, [Credential]>?) {
        if isFiltering() {
            filteredCredentials = credentials
        } else {
            allCredentials = credentials
        }
        tableView.reloadData()
    }
}


extension HomeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Do something
    }
}

extension HomeVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return isFiltering() ? (filteredCredentials?.count ?? 0): (allCredentials?.count ?? 0)
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var keys: [String] = []
        if let fCreds = filteredCredentials {
            keys = fCreds.keys.sorted()
        }
        if let creds = allCredentials {
            keys = creds.keys.sorted()
        }
        return keys[section]
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var keys: [String] = []
        if isFiltering() {
            if let fCreds = filteredCredentials {
                keys = fCreds.keys.sorted()
                return fCreds[keys[section]]?.count ?? 0
            }
        } else {
            if let creds = allCredentials {
                keys = creds.keys.sorted()
                return creds[keys[section]]?.count ?? 0
            }
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {
            let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
            return cell
        }
        var keys: [String] = []
        if isFiltering() {
            if let fCreds = filteredCredentials {
                keys = fCreds.keys.sorted()
                let key = keys[indexPath.section]
                let row = indexPath.row
                cell.textLabel?.text = fCreds[key]?[row].title ?? ""
                cell.detailTextLabel?.text = fCreds[key]?[row].expiration?.asString ?? ""
            }
        } else {
            if let creds = allCredentials {
                keys = creds.keys.sorted()
                let key = keys[indexPath.section]
                let row = indexPath.row
                cell.textLabel?.text = creds[key]?[row].title ?? ""
                cell.detailTextLabel?.text = creds[key]?[row].expiration?.asString ?? ""
            }
        }
        return cell
    }
    
    
}
