//
//  AddVC.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import UIKit

class AddVC: UIViewController {
    
    // MARK: - Properties
    var detailCredential: Credential?
    private var expDate: Date?
    private var isOrg = false
    private var frontImage: Data?
    private var backImage: Data?
    
    // MARK: - Outlets
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var vcTitleLabel: UILabel!
    @IBOutlet weak var chooseTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var idNumberTF: UITextField!
    @IBOutlet weak var expirationButton: UIButton!
    @IBOutlet weak var reminderSwitch: UISwitch!
    @IBOutlet weak var numOfDaysTF: UITextField!
    @IBOutlet weak var addFront: UIButton!
    @IBOutlet weak var deleteFront: UIButton!
    @IBOutlet weak var addBack: UIButton!
    @IBOutlet weak var deleteBack: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: - Actions
    @IBAction func chooseTypeChanged(_ sender: Any) {
        isOrg = chooseTypeSegmentedControl.selectedSegmentIndex == 0 ? true : false
    }
    @IBAction func titleTextChanged(_ sender: Any) {
    }
    @IBAction func expirationButton(_ sender: Any) {
    }
    @IBAction func reminderSwitch(_ sender: Any) {
    }
    @IBAction func addFront(_ sender: Any) {
    }
    @IBAction func deleteFront(_ sender: Any) {
    }
    @IBAction func addBack(_ sender: Any) {
    }
    @IBAction func deleteBack(_ sender: Any) {
    }
    @IBAction func saveButton(_ sender: Any) {
        saveTapped()
    }
    
    
    // MARK: - View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - Methods
    private func populateUI() {
        guard detailCredential != nil else {
            self.vcTitleLabel.text = "Add"
            return
        }
        vcTitleLabel.text = "Edit"
        chooseTypeSegmentedControl.selectedSegmentIndex = detailCredential?.isOrg == true ? 0 : 1
        titleTF.text = detailCredential?.title ?? ""
        idNumberTF.text = detailCredential?.idNumber ?? ""
        // Set expiratoin date from Detail Cred
        // Set front image from detail cred data
        // Set back image from detail cred data
        reminderSwitch.setOn(detailCredential?.hasNotifications ?? false, animated: false)
        numOfDaysTF.text = detailCredential?.notifyDaysInAdvance ?? ""
    }
    
    private func saveRequirementsMet() -> Bool {
        return titleRequirementsMet() && dateRequirementsMet() && notificationRequirementsMet()
    }
    
    private func titleRequirementsMet() -> Bool {
        // Ensure title has appropriate number of characters
        // Check title for special characters
        return true
    }
    
    private func dateRequirementsMet() -> Bool {
        // Check date is not in past
        return true
    }
    
    private func notificationRequirementsMet() -> Bool {
        // If notifications, make sure to ask for permission
        // Check if notifications are set, there is a date
        // Check if notifications are set, there is a number of days before
        return true
    }
    
    private func saveTapped() {
        guard saveRequirementsMet() else { return }
        
        let id = detailCredential?.id ?? nil
        let isOrg = self.isOrg != detailCredential?.isOrg ? self.isOrg : detailCredential?.isOrg
        let title = (titleTF.text != detailCredential?.title) ? titleTF.text : detailCredential?.title
        let idNumber = (idNumberTF.text != detailCredential?.idNumber) ? idNumberTF.text : detailCredential?.idNumber
        let expDate = (self.expDate != detailCredential?.expiration) ? self.expDate : detailCredential?.expiration
        let frontImageData = frontImage
        let backImageData = backImage
        let hasNotifications = self.reminderSwitch.isOn
        let notifyDaysInAdvance = (numOfDaysTF.text != detailCredential?.notifyDaysInAdvance) ? numOfDaysTF.text : detailCredential?.notifyDaysInAdvance
        
        var credential = Credential(id: id,
                                    isOrg: isOrg,
                                    title: title,
                                    idNumber: idNumber,
                                    expiration: expDate,
                                    frontImageData: frontImageData,
                                    backImageData: backImageData,
                                    hasNotifications: hasNotifications,
                                    notifyDaysInAdvance: notifyDaysInAdvance)
        credential.delegate = self
        credential.save()
    }
}

extension AddVC: CredentialDelegate {
    func saveComplete(success: Bool) {
        if success {
            
        } else {
            
        }
    }
    
    func imageRetrieved(success: Bool) {
        if success {
            
        } else {
            
        }
    }
    
    func imageReplaced(success: Bool) {
        // ...
    }
}
