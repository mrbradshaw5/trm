//
//  LoadingVC.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//            self.present(UIStoryboard.homeVC, animated: true)
        }
    }

}
