//
//  LoginVC.swift
//  trm
//
//  Created by Matthew Bradshaw on 2/23/20.
//  Copyright © 2020 Be Prepared Education. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    // MARK: - Properties
    
    
    
    // MARK: - Outlets
    @IBOutlet weak var organizationID: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var forgotPassword: UIButton!
    
    
    // MARK: - Actions
    @IBAction func loginButton(_ sender: Any) {
        loginTapped()
    }
    @IBAction func forgotPasswordButton(_ sender: Any) {
        forgotPasswordTapped()
    }
    
    
    // MARK: - View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - Methods
    func loginTapped() {
//        present(UIStoryboard.loadingVC, animated: true)
    }
    
    func forgotPasswordTapped() {
        
    }

}

